Cara menjalankan aplikasi:

1.  Menggunakan docker:
- Jalankan docker desktop 
- Masuk kedalam directory yang terdapat file docker-compose
- Gunakan command "docker-compose up -d"

2.  Manual 
- Extract file
- Masuk ke directory aplikasi
- perlu postgresql database dengan port 5433
- Gunakan command "mvn clean spring-boot run" (untuk mvn)

API :
- API documentation dapat dilihat dengan swagger di : http://localhost:<port:5433>/swagger-ui/index.html
- seluruh api kecuali swagger perlu menggunakan basic authentication dengan base64(email:password)
- Dapat menggunakan felix@gmail.com:password sebagai default user
