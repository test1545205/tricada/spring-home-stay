package com.test.notificationservice.handler;

import com.test.notificationservice.event.BillingEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BillingHandler {

    @KafkaListener(topics = "notificationBilling")
    public void handleNotification(BillingEvent event){
        log.info("notifikasi telah diterima untuk billing dengan id : " + event.getId());
    }

}
