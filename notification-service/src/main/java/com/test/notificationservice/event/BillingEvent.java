package com.test.notificationservice.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BillingEvent {
    private Long id;
    private Long roomId;
    private Long userId;
    private BigDecimal roomPrice;
    private BigDecimal facilitiesPrice;
    private BigDecimal totalPrice;
}
