package com.test.homestay.controller;

import com.test.homestay.dto.interfaceDto.IReservationDTO;
import com.test.homestay.service.BillingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/billing")
@RequiredArgsConstructor
public class BillingController {

    private final BillingService billingService;

    @GetMapping("/{id}")
    public ResponseEntity<IReservationDTO> findBillingById(
            @PathVariable("id") Long id
    ){
        return ResponseEntity.ok(billingService.findBillingById(id));
    }

}
