package com.test.homestay.controller;

import com.test.homestay.entity.User;
import com.test.homestay.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<User> register(
            @RequestBody User user
    ){
        return ResponseEntity.ok(userService.save(user));
    }

    @GetMapping
    public ResponseEntity<List<User>> getAll(){
        return ResponseEntity.ok(userService.list());
    }

    @GetMapping("/email")
    public ResponseEntity<User> findUserByEmail(
            @RequestParam(name = "email") String email
    ){
        return ResponseEntity.ok(userService.findByEmail(email));
    }

}
