package com.test.homestay.controller;

import com.test.homestay.dto.ReservationDto;
import com.test.homestay.dto.interfaceDto.IReservationDTO;
import com.test.homestay.entity.Reservation;
import com.test.homestay.service.ReservationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/reservation")
@RequiredArgsConstructor
public class ReservationController {

    private final ReservationService reservationService;

    @GetMapping
    public ResponseEntity<List<Reservation>> getAll(){
        return ResponseEntity.ok(reservationService.list());
    }

    @PostMapping
    public ResponseEntity<Reservation> save(
            @RequestBody ReservationDto.Save reservation
    ){
        return ResponseEntity.ok(reservationService.save(reservation));
    }

    @PutMapping("/{id}/checkin")
    public void updateCheckIn(
            @PathVariable("id") Long id
    ){
        System.out.println("id checkin ----------> " + id);
        int updateStatus = reservationService.updateCheckIn(id);
        System.out.println("update status integer ----------> " + updateStatus);
    }

    @PutMapping("/{id}/checkout")
    public void updateCheckOut(
            @PathVariable("id") Long id
    ){
        int updateStatus = reservationService.updateCheckOut(id);
        System.out.println("update status integer ----------> " + updateStatus);
    }

}
