package com.test.homestay.service;

import com.test.homestay.dto.ReservationDto;
import com.test.homestay.entity.Facility;
import com.test.homestay.entity.Reservation;
import com.test.homestay.entity.Room;
import com.test.homestay.entity.User;
import com.test.homestay.repository.ReservationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ReservationService {

    private final ReservationRepository reservationRepository;
    private final UserService userService;
    private final RoomService roomService;
    private final FacilityService facilityService;

    public Reservation save (ReservationDto.Save reservationUser){

        User user = userService.findById(reservationUser.getUserId());
        Room room = roomService.findById(reservationUser.getRoomId());

        Set<Facility> facilities = new HashSet<>();
        reservationUser.getFacilities().forEach(facility -> {
            Facility fac = facilityService.findById(facility);
            facilities.add(fac);
        });

        Reservation reservation = new Reservation(null, user, room,facilities, null, null, "created");

        return reservationRepository.save(reservation);
    }

    public List<Reservation> list (){
        return reservationRepository.findAll();
    }

    public int updateCheckIn(Long id){
        System.out.println("service --------> " + id);
        return reservationRepository.updateCheckIn(new Date(), id);
    }

    public int updateCheckOut(Long id){
        return reservationRepository.updateCheckOut(new Date(), id);
    }

}
