package com.test.homestay.service;

import com.test.homestay.entity.User;
import com.test.homestay.exception.IdNotFoundException;
import com.test.homestay.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findByEmail(username).orElseThrow(() ->
                new UsernameNotFoundException(username));

        UserDetails userDetails = org.springframework.security.core.userdetails.User.withUsername(user.getEmail()).authorities("ROLE_USER").password(user.getPassword()).build();
        return userDetails;
    }

    public List<User> list(){
        return userRepository.findAll();
    }

    public User save(User user){
        return userRepository.save(user);
    }

    public User findById(Long id){
        return userRepository.findById(id).orElseThrow(() ->
                new IdNotFoundException("User dengan id " + id + " tidak ditemukan"));
    }

    public User findByEmail(String email){
        return userRepository.findByEmail(email).orElseThrow(() ->
                new UsernameNotFoundException("User dengan email " + email + " tidak ditemukan"));
    }
}
