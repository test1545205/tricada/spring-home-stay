package com.test.homestay.service;

import com.test.homestay.dto.interfaceDto.IReservationDTO;
import com.test.homestay.event.BillingEvent;
import com.test.homestay.exception.IdNotFoundException;
import com.test.homestay.repository.ReservationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BillingService {

    private final ReservationRepository reservationRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public IReservationDTO findBillingById(Long id){
        IReservationDTO reservationDTO = reservationRepository.findBillingById(id).orElseThrow(() ->
                new IdNotFoundException("Billing dengan id reservasi " + id + " tidak ditemukan"));

        BillingEvent billingEvent = new BillingEvent();
        billingEvent.setId(reservationDTO.getId());
        billingEvent.setRoomId(reservationDTO.getRoomId());
        billingEvent.setUserId(reservationDTO.getUserId());
        billingEvent.setRoomPrice(reservationDTO.getRoomPrice());
        billingEvent.setFacilitiesPrice(reservationDTO.getFacilitiesPrice());

        kafkaTemplate.send("notificationBilling", billingEvent);
        return reservationDTO;
    }

}
