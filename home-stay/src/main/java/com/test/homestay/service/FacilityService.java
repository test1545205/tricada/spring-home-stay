package com.test.homestay.service;

import com.test.homestay.entity.Facility;
import com.test.homestay.exception.IdNotFoundException;
import com.test.homestay.repository.FacilityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FacilityService {

    private final FacilityRepository facilityRepository;

    public Facility save(Facility facility){
        return facilityRepository.save(facility);
    }

    public List<Facility> list (){
        return facilityRepository.findAll();
    }

    public Facility findById(Long id){
        return facilityRepository.findById(id).orElseThrow(() ->
                new IdNotFoundException("Facility dengan id " + id + " tidak ditemukan"));
    }

}
