package com.test.homestay.service;

import com.test.homestay.entity.Room;
import com.test.homestay.exception.IdNotFoundException;
import com.test.homestay.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoomService {

    private final RoomRepository roomRepository;

    public Room save(Room room){
        return roomRepository.save(room);
    }

    public List<Room> list(){
        return roomRepository.findAll();
    }

    public Room findById(Long id){
        return roomRepository.findById(id).orElseThrow(() ->
                new IdNotFoundException("Room dengan id " + id + " tidak ditemukan"));
    }

}
