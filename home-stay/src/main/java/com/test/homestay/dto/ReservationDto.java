package com.test.homestay.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

public class ReservationDto {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Save{
        private Long userId;
        private Long roomId;
        private Set<Long> facilities;
    }

}
