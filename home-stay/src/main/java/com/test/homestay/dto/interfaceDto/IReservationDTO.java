package com.test.homestay.dto.interfaceDto;

import java.math.BigDecimal;

public interface IReservationDTO {

    Long getId();
    Long getRoomId();
    Long getUserId();
    BigDecimal getRoomPrice();
    BigDecimal getFacilitiesPrice();
    BigDecimal getTotalPrice();

}
