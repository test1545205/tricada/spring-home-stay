package com.test.homestay;

import com.test.homestay.dto.ReservationDto;
import com.test.homestay.entity.Facility;
import com.test.homestay.entity.Room;
import com.test.homestay.entity.User;
import com.test.homestay.service.FacilityService;
import com.test.homestay.service.ReservationService;
import com.test.homestay.service.RoomService;
import com.test.homestay.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class HomeStayApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeStayApplication.class, args);
	}

	@Bean
	CommandLineRunner run(RoomService roomService, UserService userService, FacilityService facilityService, ReservationService reservationService){
		return args -> {

//			GIVE SOME INITIAL DATA
			Room singleRoom = roomService.save(new Room(null, "single", "paling murah", new BigDecimal(200000)));
			Room twinRoom = roomService.save(new Room(null, "twin", "twin room", new BigDecimal(300000)));

			User user = userService.save(new User(null, "felix", "felix@gmail.com", "password"));

			Facility breakfastFac = facilityService.save(new Facility(null, "breakfast", "makan pagi", new BigDecimal(25000)));
			Facility extraBedFac = facilityService.save(new Facility(null, "extra bed", "kasur tambahan", new BigDecimal(30000)));

			Set<Long> facilities = new HashSet<>();
			facilities.add(breakfastFac.getId());
			facilities.add(extraBedFac.getId());
			reservationService.save(new ReservationDto.Save(user.getId(), singleRoom.getId(), facilities));
		};
	}

}
