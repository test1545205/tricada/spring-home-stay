package com.test.homestay.repository;

import com.test.homestay.dto.interfaceDto.IReservationDTO;
import com.test.homestay.entity.Reservation;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Query(value = """
            SELECT\s
            id,\s
            room_id as roomId,\s
            user_id as userId,
            room_price as roomPrice,
            facility_price as facilitiesPrice,
            room_price + facility_price as totalPrice
            FROM (
            	
            	SELECT\s
            	r.id as id,\s
            	check_in_date,\s
            	check_out_date,\s
            	status,\s
            	room_id,\s
            	user_id,\s
            	(select r2.price from public.room r2 where r2.id = room_id) as room_price,\s
            	sum(f.price) as facility_price
            	FROM public.reservation r\s
            	join public.reservation_facility rf on r.id = rf.res_id\s
            	join public.facility f on rf.facility_id = f.id
            	where r.id = :id
            	group by r.id
                        
            ) as reservation_view
            """, nativeQuery = true)
    Optional<IReservationDTO> findBillingById(@Param("id") Long id);

    @Modifying
    @Transactional
    @Query(value = """
            UPDATE public.reservation
            SET check_in_date=:checkInDate , status='open'
            WHERE id=:id
            """, nativeQuery = true)
    int updateCheckIn(@Param("checkInDate") Date checkInDate, @Param("id") Long id);

    @Modifying
    @Transactional
    @Query(value = """
            UPDATE public.reservation
            SET check_out_date=:checkOutDate , status='closed'
            WHERE id=:id
            """, nativeQuery = true)
    int updateCheckOut(@Param("checkOutDate") Date checkOutDate, @Param("id") Long id);

}
