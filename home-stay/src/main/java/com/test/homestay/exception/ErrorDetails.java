package com.test.homestay.exception;

public class ErrorDetails {

    private boolean status;
    private String message;

    public ErrorDetails(boolean status, String message) {

        super();

        this.status = status;

        this.message = message;

    }

    public boolean getStatus() {

        return status;

    }

    public String getMessage() {

        return message;

    }
}
