CREATE TABLE IF NOT EXISTS public.users (
	id int8 NOT NULL,
	email varchar(255) NULL,
	"name" varchar(255) NULL,
	"password" varchar(255) NULL,
	CONSTRAINT users_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.room (
	id int8 NOT NULL,
	description varchar(255) NULL,
	"name" varchar(255) NULL,
	price numeric(38,2) NULL,
	CONSTRAINT room_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.facility (
	id int8 NOT NULL,
	description varchar(255) NULL,
	"name" varchar(255) NULL,
	price numeric(38,2) NULL,
	CONSTRAINT facility_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.reservation (
	id int8 NOT NULL,
	check_in_date timestamp NULL,
	check_out_date timestamp NULL,
	status varchar(255) NULL,
	room_id int8 REFERENCES public.room (id),
	user_id int8 REFERENCES public.users (id),
	CONSTRAINT reservation_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.reservation_facility (
	res_id int8 REFERENCES public.reservation (id),
	facility_id int8 REFERENCES public.facility (id),
	CONSTRAINT reservation_facility_pkey null
);